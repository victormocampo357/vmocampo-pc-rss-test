<?php

namespace Tests\Unit;

use App\Models\RssPodcast;
use Tests\TestCase;
use App\Services\RSS\Import as RssImport;
use App\Services\RSS\RssPodcastAdapter;

class RssConsumeTest extends TestCase
{
    /**
     * This feed should always be available, but ideally would make a custom feed locally to test
     *
     * @var string
     */
    private $testRssFeed = "https://rss.art19.com/apology-line";

    /**
     * Test downloading an RSS feed into an XML object, parsing it into a model fillable.
     * Should save to the database -- then we'll remove it as cleanup
     * 
     * @return void
     */
    public function test_rss_import_to_adapter()
    {
        $rssImport = new RssImport($this->testRssFeed);
        $rssContentArray = $rssImport->getRssContentArray();
        $this->assertArrayHasKey('channel', $rssContentArray);

        $rssPodcastAdapter = new RssPodcastAdapter();
        $rssPodcastAdapter->adapt($rssContentArray);
        $this->assertArrayHasKey('title', $rssPodcastAdapter->output);

        $rssPodcast = new RssPodcast($rssPodcastAdapter->output);
        $this->assertTrue($rssPodcast->save());

        $rssPodcast->delete();
    }
}
