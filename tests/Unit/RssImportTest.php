<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\RSS\Import as RssImport;

class RssImportTest extends TestCase
{
    /**
     * This feed should always be available, but ideally would make a custom feed locally to test
     *
     * @var string
     */
    private $testRssFeed = "https://rss.art19.com/apology-line";

    /**
     * Test connection and response code is working
     * 
     * @return void
     */
    public function test_rss_download()
    {
        $rssImport = new RssImport($this->testRssFeed);
        $this->assertTrue($rssImport->isValidRssFeed());
        $this->assertIsInt($rssImport->getStatusCode());
        $this->assertIsString($rssImport->getResponseBody());
    }

    /**
     * Test that we are able to parse into an array correctly
     *
     * @return void
     */
    public function test_rss_parse() {
        $rssImport = new RssImport($this->testRssFeed);
        $rssContentArray = $rssImport->getRssContentArray();
        $this->assertIsArray($rssContentArray);
        $this->assertArrayHasKey('channel', $rssContentArray);
    }
}
