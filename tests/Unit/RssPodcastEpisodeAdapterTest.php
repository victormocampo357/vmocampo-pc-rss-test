<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\RSS\RssPodcastEpisodeAdapter;

class RssPodcastEpisodeAdapterTest extends TestCase
{
    /**
     * Test our adapter's lookup table against a given array,
     * single dimensional
     *
     * @return void
     */
    public function test_lookup_adapt_single()
    {
        $rssPodcastEpisodeAdapter = new RssPodcastEpisodeAdapter();

        $mockArray = [
            'title' => 'Valid Title'
        ];

        $rssPodcastEpisodeAdapter->adapt($mockArray);

        $this->assertEquals('Valid Title', $rssPodcastEpisodeAdapter->output['title']);
    }

    /**
     * Test our adapter's lookup table against a given array,
     * multi dimensional
     *
     * @return void
     */
    public function test_lookup_adapt_multi()
    {
        $rssPodcastEpisodeAdapter = new RssPodcastEpisodeAdapter();

        $mockArray = [
            'enclosure' => [
                '@attributes' => [
                    'url' => 'Valid URL'
                ]
            ]
        ];

        $rssPodcastEpisodeAdapter->adapt($mockArray);

        $this->assertEquals('Valid URL', $rssPodcastEpisodeAdapter->output['audio_url']);
    }
}
