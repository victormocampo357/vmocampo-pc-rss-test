<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\RSS\RssPodcastAdapter;

class RssPodcastAdapterTest extends TestCase
{
    /**
     * Test our adapter's lookup table against a given array,
     * single dimensional
     *
     * @return void
     */
    public function test_lookup_adapt_single()
    {
        $rssPodcastAdapter = new RssPodcastAdapter();

        $mockArray = [
            'title' => 'Valid Title'
        ];

        $rssPodcastAdapter->adapt($mockArray);

        $this->assertEquals('Valid Title', $rssPodcastAdapter->output['title']);
    }

    /**
     * Test our adapter's lookup table against a given array,
     * multi dimensional
     *
     * @return void
     */
    public function test_lookup_adapt_multi()
    {
        $rssPodcastAdapter = new RssPodcastAdapter();

        $mockArray = [
            'image' => [
                'url' => 'Valid URL'
            ]
        ];

        $rssPodcastAdapter->adapt($mockArray);

        $this->assertEquals('Valid URL', $rssPodcastAdapter->output['artwork_url']);
    }
}
