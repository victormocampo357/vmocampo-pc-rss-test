<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RssPodcastEpisode extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rss_podcast_episode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "title", "description", "audio_url", "episode_url"
    ];

    /**
     * Get the corresponding RssPodcast this episode is part of
     */
    public function rssPodcast()
    {
        return $this->belongsTo(RssPodcast::class, 'rss_podcast_id');
    }
}
