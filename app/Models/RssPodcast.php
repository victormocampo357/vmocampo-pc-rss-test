<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RssPodcast extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rss_podcast';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "title", "language", "description", "artwork_url", "rss_feed_url"
    ];

    /**
     * Get episodes belonging to this podcast
     */
    public function episodes()
    {
        return $this->hasMany(RssPodcastEpisode::class);
    }
}
