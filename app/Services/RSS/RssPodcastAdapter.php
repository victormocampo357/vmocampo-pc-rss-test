<?php

namespace App\Services\RSS;

/**
 * RSS Podcast Adapter
 *
 * Adapt any inbound array into an \App\Models\RssPodcast fillable array
 *
 */ 
class RssPodcastAdapter extends Adapter
{       
    /**
     * Initialize the lookup table (ideally would be a db driven model)
     *
     * @return void
     */
    public function __construct() {
        $this->lookup = [
            "title" => ['title' => []], 
            "language" => ['language' => [], 'lang' => []], 
            "description" => ['description' => []], 
            "artwork_url" => ['image' => ['url' => [], 'link' => []]],
            "rss_feed_url" => ['link' => [], 'url' => [], 'generator' => []]
        ];
    }
}