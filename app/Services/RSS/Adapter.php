<?php

namespace App\Services\RSS;

/**
 * RSS Podcast Adapter Abstract
 *
 * Adapt any inbound array into any given Model fillable
 *
 */ 
abstract class Adapter
{
        /**
     * Store the lookup table for matching fields
     *
     * @var array
     */
    public $lookup;
    
    /**
     * A fillable array for our RSS Podcast model
     *
     * @var array
     */
    public $output;

    /**
     * Adapt any given array to the lookup table keys
     * Missing matches are converted to empty strings.
     * Keys are in order of match priority
     *
     * @param  mixed $array
     * @return void
     */
    public function adapt($array) {
        foreach ($this->lookup as $fillableKey => $validFields) {                        
            $this->output[$fillableKey] = "No value";            
            $this->match($fillableKey, $array, $validFields);
        }
    }
    
    /**
     * Recursive function to traverse our lookup table
     * If a given field in a lookup table is set on the target array,
     * we check to see if its a non array value.
     * 
     * Non array values are saved in the output in the field defined by
     * key.
     * 
     * Array values are recursively checked until they reach a non array
     * result in the target array. We assume that the lookup match also had
     * a sub array, but we use empty array fields to indicate that although the
     * match had an array, our lookup did not and we end there.
     *
     * @param  string $key
     * @param  array $array
     * @param  array $validFields
     * @return void
     */
    public function match($key, $array, $validFields) {        
        foreach (array_keys($validFields) as $lookupField) {
            if (isset($array[$lookupField]) && !empty($array[$lookupField])) {                
                if (is_array($array[$lookupField])) {
                    $this->match($key, $array[$lookupField], $validFields[$lookupField]);
                } else {
                    $this->output[$key] = $array[$lookupField];
                    break;
                }                                
            }
        }
    }
}