<?php

namespace App\Services\RSS;

/**
 * RSS Podcast Adapter
 *
 * Adapt any inbound array into an \App\Models\RssPodcastEpisode fillable array
 *
 */ 
class RssPodcastEpisodeAdapter extends Adapter
{       
    /**
     * Initialize the lookup table (ideally would be a db driven model)
     *
     * @return void
     */
    public function __construct() {
        $this->lookup = [
            "title" => ['title' => []], 
            "description" => ['description' => []],
            "audio_url" => [
                'enclosure' => [
                    '@attributes' => ['url' => []]], 
                    'link' => [], 
                    'url' => []
                ],              
            "episode_url" => ['link' => [], 'url' => []],            
        ];
    }
}