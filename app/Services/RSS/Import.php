<?php

namespace App\Services\RSS;

/**
 * RSS Import Class
 *
 * Store short-term connection info, consume XML and perform transformational actions. 
 *
 */ 
class Import
{
    /**
     * @var $url
     * 
     * RSS Feed URL
     */
    public $url = "";

    /**
     * @var string $responseBody
     * 
     * XML RSS raw data
     */
    public $responseBody = "";

    /**
     * @var $rssContentArray
     * 
     * If we have a valid RSS feed, this will store an XML object we can parse
     */
    public $rssContentArray = null;

    /**
     * @var $statusCode
     * 
     * Store the HTTP response code for easy access
     */
    public $statusCode = 0;

    /**
     * Construct.
     *
     * @param string $feed_url
     * @param integer $repeat How many times something interesting should happen
     * 
     * @return void
     */ 
    public function __construct($feed_url="") {
        if (!$feed_url) {
            throw new \Exception('RSS Import feed URL must be provided!');
        }
        $this->url = $feed_url;
        $this->download();
        $this->parseIntoArray();
    }

    /**
     * Download the data from the given URL and store it locally.
     *
     * @return void
     */ 
    public function download() {        
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->url);        
        $this->statusCode = $response->getStatusCode();
        $this->responseBody = $response->getBody()->getContents();
    }

    /**
     * Parse the downloaded response body into a PHP array we can traverse
     * 
     * @return void
     */
    public function parseIntoArray() {
        $this->rssContentArray = json_decode(json_encode((array) simplexml_load_string($this->responseBody)), true);
    }

    /**
     * Use the status code and XML formatting results
     * 
     * @return bool
     */
    public function isValidRssFeed() {
        return ($this->statusCode == 200 && $this->responseBody && isset($this->rssContentArray['channel']));
    }

    /**
     * Getter responseBody
     *
     * @return string
     */
    public function getResponseBody() {
        return $this->responseBody;
    }

    /**
     * Getter statusCode
     *
     * @return int
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Getter contentArray
     *
     * @return array
     */
    public function getRssContentArray() {
        return $this->rssContentArray;
    }
}