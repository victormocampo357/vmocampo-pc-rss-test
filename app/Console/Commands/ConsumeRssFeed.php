<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RssPodcast;
use App\Models\RssPodcastEpisode;
use App\Services\RSS\Import as RssImport;
use App\Services\RSS\RssPodcastAdapter;
use App\Services\RSS\RssPodcastEpisodeAdapter;

class ConsumeRssFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:consume {feed_url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will download information from a valid RSS feed and persist it to our database layer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rssFeedUrl = $this->argument('feed_url');

        try {
            $rssImport = new RssImport($rssFeedUrl);
        } catch(\GuzzleHttp\Exception\ConnectException $e) {
            $this->error('Unable to connect to feed: ' . $e->getMessage());
            return;
        } catch(\GuzzleHttp\Exception\ClientException $e) {
            $this->error('Feed unreachable: ' . $e->getMessage());
            return;
        } catch(\Throwable $e) {
            $this->error('A general error occured' . $e->getMessage());
            return;
        }
                        
        $this->info("Processing RSS feed... [{$rssFeedUrl}]");

        if (!$rssImport->isValidRssFeed()) {
            $this->error('RSS feed invalid, please provide a different URL.');
            return;
        }

        $rssPodcastAdapter = new RssPodcastAdapter();
        $rssPodcastAdapter->adapt($rssImport->rssContentArray["channel"]);
        $rssPodcast = new RssPodcast($rssPodcastAdapter->output);

        try {
            $rssPodcast->save();        
        } catch (\Throwable $e) {
            $this->error('Unable to save RSS Podcast. Exiting...');
            return;
        }
        
        $rssPodcastEpisodeAdapter = new RssPodcastEpisodeAdapter();
        if (isset($rssImport->rssContentArray["channel"]["item"])) {
            $this->info(sprintf("Importing [%s] episodes...", count($rssImport->rssContentArray["channel"]["item"])));
            foreach ($rssImport->rssContentArray["channel"]["item"] as $episode) {
                $rssPodcastEpisodeAdapter->adapt($episode);
                $rssPodcastEpisode = new RssPodcastEpisode($rssPodcastEpisodeAdapter->output);
                $rssPodcastEpisode->rss_podcast_id = $rssPodcast->id;
                try {
                    $rssPodcastEpisode->save();
                } catch (\Throwable $e) {
                    $this->error(sprintf("\t Unable to save RSS Podcast Episode [%s]", $rssPodcastEpisode->title));
                }                
            }
        }        

        $this->info('Finished;');
    }
}
