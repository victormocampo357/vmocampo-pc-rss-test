<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssPodcastEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        // A table for episodes, with columns for the episode's title, description, audio URL, and episode URL (if it has one)
        Schema::create('rss_podcast_episode', function (Blueprint $table) {
            $table->id();
            $table->tinyText('title');
            $table->longText('description');
            $table->text('audio_url');
            $table->text('episode_url')->nullable();
            $table->unsignedBigInteger('rss_podcast_id');
            $table->foreign('rss_podcast_id')->references('id')->on('rss_podcast');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_podcast_episode');
    }
}
