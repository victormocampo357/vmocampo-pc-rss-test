<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssPodcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // A table for podcasts, with columns for the podcast's title, artwork URL, RSS feed URL, description, language, and website URL        
        Schema::create('rss_podcast', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->tinytext('title');
            $table->text('artwork_url');
            $table->text('rss_feed_url');
            $table->longText('description');
            $table->tinytext('language');
            $table->text('website_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_podcast');
    }
}
